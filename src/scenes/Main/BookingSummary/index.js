import React, { Component, PropTypes } from 'react';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import {
    TouchableWithoutFeedback,
    Alert,
    Image,
} from 'react-native';
import {
    Container,
    Header,
    Title,
    InputGroup,
    Input,
    Button,
    Icon,
    Text,
    View,
    Spinner,
    Body,
    Content,
} from 'native-base';
import validator from 'validator';
import { width, height, totalSize } from 'react-native-dimension';
import Switch from 'react-native-switch-pro';
import AndroidBackButton from 'react-native-android-back-button';
import SimpleStepper from 'react-native-simple-stepper'
import FormMessage from 'Vendor/src/components/FormMessage';
import * as session from 'Vendor/src/services/session';
import * as api from 'Vendor/src/services/api';

class BookingSummary extends Component {
    static propTypes = {
        navigator: PropTypes.shape({
            getCurrentRoutes: PropTypes.func,
            jumpTo: PropTypes.func,
        }),
    }

    constructor(props) {
        super(props);

        this.initialState = {
            isLoading: false,
            error: null,
            email: '',
            password: '',
        };
        this.state = this.initialState;
    }

    onPressBack() {
        const routeStack = this.props.navigator.getCurrentRoutes();
        this.props.navigator.jumpTo(routeStack[10]);
    }

    onPressForward() {
        const routeStack = this.props.navigator.getCurrentRoutes();
        this.props.navigator.jumpTo(routeStack[12]);
    }

    renderError() { 
        if (this.state.error) {
            return (
                <Text
                    style={{ color: 'red', marginBottom: 20 }}
                >
                    {this.state.error}
                </Text>
            );
        }
    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor:'#3CA1F2'}}>
                    <Button
                        onPress={() => this.onPressBack()}
                        title="Learn More"
                        transparent
                    >
                        <Icon name="ios-arrow-back" style={{ color:'#F9FCFE'}}/>
                        <Text style={{ color:'#F9FCFE'}}>Back</Text>
                    </Button>
                    <Body>
                        <Title style={{ color: '#F9FCFE' }}>Booking Summary</Title>
                    </Body>
                    <Button
                        onPress={() => this.onPressForward()}
                        transparent
                    >
                        <Text style={{ color: '#F9FCFE' }}>Search</Text>
                    </Button>
                </Header>
                <Content style={{backgroundColor: '#F9FCFE'}}>
                        <View>
                            <Text style={{ marginTop: 20, alignSelf: 'center', justifyContent: 'center', fontSize: 20, fontWeight: 'bold' }} transparent>
                                Summary of your request
                            </Text>
                            <View>
                                <Text style={{ fontSize: 15, fontWeight: 'bold', padding: 10, paddingLeft: 20, paddingRight: 20 }}>Origin</Text>
                                <View
                                    style={{
                                        borderBottomWidth: 1,
                                        left: 20,
                                        borderBottomColor: '#F3F3F3',
                                        width: 400,
                                    }}
                                />
                                <View style={{ paddingLeft: 20, paddingRight: 20, flexDirection: 'column' }}>
                                    <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, color: '#989898' }}>27 Maranda Place</Text>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'right', color: '#989898' }}>Winnipeg.MB</Text>
                                    </View>
                                    <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, color: '#989898' }}>Building Type</Text>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'center', color: '#989898' }}>2 Stairs</Text>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'right', color: '#989898' }}>1 Rooms</Text>
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{
                                    borderBottomWidth: 1,
                                    left: 20,
                                    borderBottomColor: '#F3F3F3',
                                    width: 400,
                                }}
                            />
                            <View >
                                <Text style={{ fontSize: 15, fontWeight: 'bold', padding: 10, paddingLeft: 20, paddingRight: 20 }}>Destination</Text>
                                <View
                                    style={{
                                        borderBottomWidth: 1,
                                        left:20,
                                        borderBottomColor: '#F3F3F3',
                                        width: 400,
                                    }}
                                />
                                <View style={{ paddingLeft: 20, paddingRight: 20, flexDirection: 'column' }}>
                                    <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                    <Text style={{ padding: 10, flex: 1, fontSize: 12, color:'#989898' }}>27 Maranda Place</Text>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'right', color:'#989898'  }}>Winnipeg.MB</Text>
                                    </View>
                                    <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, color:'#989898' }}>Building Type</Text>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'center', color:'#989898'  }}>2 Stairs</Text>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'right', color:'#989898'  }}>1 Rooms</Text>
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{
                                    borderBottomWidth: 1,
                                    left: 20,
                                    borderBottomColor: '#F3F3F3',
                                    width: 400,
                                }}
                            />
                            <View>
                                <Text style={{ fontSize: 15, fontWeight: 'bold', padding: 10, paddingLeft: 20, paddingRight: 20 }}>Date Move</Text>
                                <View
                                    style={{
                                        borderBottomWidth: 1,
                                        left:20,
                                        borderBottomColor: '#F3F3F3',
                                        width: 400,
                                    }}
                                />
                                <View style={{ paddingLeft: 20, paddingRight: 20, flexDirection: 'column' }}>
                                    <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                    <Text style={{ padding: 10, flex: 1, fontSize: 12, color:'#989898' }}>May 30, 2017 13:00</Text>
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{
                                    borderBottomWidth: 1,
                                    left: 20,
                                    borderBottomColor: '#F3F3F3',
                                    width: 400,
                                }}
                            />
                            <View >
                                <Text style={{ fontSize: 15, fontWeight: 'bold', padding: 10, paddingLeft: 20, paddingRight: 20 }}>Additional stops</Text>
                                <View
                                    style={{
                                        borderBottomWidth: 1,
                                        left: 20,
                                        borderBottomColor: '#F3F3F3',
                                        width: 400,
                                    }}
                                />
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{width:20, left:20, top:10}}>1.</Text>
                                    <View style={{ paddingRight: 20, flexDirection: 'column', flex:1 }}>
                                        <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, color: '#989898' }}>27 Maranda Place</Text>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'right', color: '#989898' }}>Winnipeg.MB</Text>
                                        </View>
                                        <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, color: '#989898' }}>Building Type</Text>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'center', color: '#989898' }}>2 Stairs</Text>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'right', color: '#989898' }}>1 Rooms</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ width: 20, left: 20, top: 10 }}>2.</Text>
                                    <View style={{ paddingRight: 20, flexDirection: 'column', flex: 1 }}>
                                        <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, color: '#989898' }}>27 Maranda Place</Text>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'right', color: '#989898' }}>Winnipeg.MB</Text>
                                        </View>
                                        <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, color: '#989898' }}>Building Type</Text>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'center', color: '#989898' }}>2 Stairs</Text>
                                            <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'right', color: '#989898' }}>1 Rooms</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{
                                    borderBottomWidth: 1,
                                    left: 20,
                                    borderBottomColor: '#F3F3F3',
                                    width: 400,
                                }}
                            />
                            <View >
                                <Text style={{ fontSize: 15, fontWeight: 'bold', padding: 10, paddingLeft: 20, paddingRight: 20 }}>Your Items</Text>
                                <View
                                    style={{
                                        borderBottomWidth: 1,
                                        left: 20,
                                        borderBottomColor: '#F3F3F3',
                                        width: 400,
                                    }}
                                />
                                <View style={{ paddingLeft: 20, paddingRight: 20, flexDirection: 'column' }}>
                                    <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, color: '#989898' }}>Maranda Place, this is a 
                                            product and text line this should not be possible to make sure
                                        </Text>
                                    </View>
                                    <View style={{ paddingLeft: 10, paddingRight: 20, flexDirection: 'row' }}>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, color: '#989898' }}>Grand Piano</Text>
                                        <Text style={{ padding: 10, flex: 1, fontSize: 12, textAlign: 'right', color: '#989898' }}>3 Items</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    <AndroidBackButton
                        onPress={() => this.onPressBack()}
                    />
                </Content>
            </Container>
        );
    }
}

export default BookingSummary;
