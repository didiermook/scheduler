import React, { Component, PropTypes } from 'react';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import {
    TouchableWithoutFeedback,
    Alert,
    Easing,
    Image,
} from 'react-native';
import {
    Container,
    Header,
    Title,
    InputGroup,
    Input,
    Button,
    Icon,
    Text,
    View,
    Spinner,
    Body,
    Content,
} from 'native-base';
import validator from 'validator';
import { width, height, totalSize } from 'react-native-dimension';
import Switch from 'react-native-switch-pro';
import Rating from 'react-native-rating'
import AndroidBackButton from 'react-native-android-back-button';
import SimpleStepper from 'react-native-simple-stepper'
import FormMessage from 'Vendor/src/components/FormMessage';
import * as session from 'Vendor/src/services/session';
import * as api from 'Vendor/src/services/api';

class SelectVendor extends Component {
    static propTypes = {
        navigator: PropTypes.shape({
            getCurrentRoutes: PropTypes.func,
            jumpTo: PropTypes.func,
        }),
    }

    constructor(props) {
        super(props);

        this.initialState = {
            isLoading: false,
            error: null,
            email: '',
            password: '',
        };
        this.state = this.initialState;
    }

    onPressBack() {
        const routeStack = this.props.navigator.getCurrentRoutes();
        this.props.navigator.jumpTo(routeStack[11]);
    }

    onPressForward() {
        const routeStack = this.props.navigator.getCurrentRoutes();
        this.props.navigator.jumpTo(routeStack[3]);
    }

    renderError() { 
        if (this.state.error) {
            return (
                <Text
                    style={{ color: 'red', marginBottom: 20 }}
                >
                    {this.state.error}
                </Text>
            );
        }
    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor:'#3CA1F2'}}>
                    <Button
                        onPress={() => this.onPressBack()}
                        title="Learn More"
                        transparent
                    >
                        <Icon name="ios-arrow-back" style={{ color:'#F9FCFE'}}/>
                        <Text style={{ color:'#F9FCFE'}}>Back</Text>
                    </Button>
                    <Body>
                        <Title style={{ color: '#F9FCFE' }}>Select a vendor</Title>
                    </Body>
                    <Button
                        onPress={() => this.onPressForward()}
                        transparent
                    >
                        <Text style={{ color: '#F9FCFE' }}>Cancel</Text>
                    </Button>
                </Header>
                <Content style={{backgroundColor: '#F9FCFE'}}>
                        <View>
                            <Text style={{ marginTop: 20, alignSelf: 'center', justifyContent: 'center', fontSize: 20, fontWeight: 'bold' }} transparent>
                                Vendors matching your request
                            </Text>
                            <Text style={{ alignSelf: 'center', justifyContent: 'center',fontSize: 12, color: '#989898' }}>
                                Here are some of the vendors based your criteria
                            </Text>
                            <View
                                style={{
                                    borderBottomWidth: 1,
                                    left: 20,
                                    borderBottomColor: '#F3F3F3',
                                    width: 400,
                                }}
                            />
                            <View style={{ flexDirection: 'row', paddingTop:5, paddingBottom:5}}>
                                <View style={{ left: 10, paddingRight: 20, alignSelf: 'center', justifyContent: 'center' }}>
                                    <Image
                                        style={{ paddingLeft:10 ,width: 40, height: 40, borderRadius: 20}}
                                        source={require('Vendor/imgs/car.png')}
                                    />
                                </View>
                                <View style={{ paddingRight: 20, flexDirection: 'column', flex: 1 }}>
                                    <Text style={{ paddingLeft: 10, paddingRight: 20, fontSize: 15}}>CWS Logistics</Text>
                                    <Text style={{ padding: 10, flex: 1, fontSize: 12, color: '#989898', paddingTop:5, paddingBottom:5}}>Winninpeg. Brandon. Edmonton</Text>
                                    <View style={{ flexDirection: 'row', padding:10, paddingTop:0, paddingBottom:0 }}>
                                        <Rating
                                            onChange={rating => console.log(rating)}
                                            selectedStar={require('Vendor/imgs/star_filled.png')}
                                            unselectedStar={require('Vendor/imgs/star_unfilled.png')}
                                            config={{
                                                easing: Easing.inOut(Easing.ease),
                                                duration: 350
                                            }}
                                            style={{justifyContent:'center'}}
                                            max={5}
                                            stagger={80}
                                            initial={5}
                                            maxScale={1}
                                            starStyle={{
                                                width: 15,
                                                height: 15
                                            }}
                                        />
                                        <Text style={{ paddingLeft:10, fontSize: 10, color: '#afb1b6', justifyContent:'center' }}>
                                            14 Reviews
						            	</Text>
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{
                                    borderBottomWidth: 1,
                                    left: 20,
                                    borderBottomColor: '#F3F3F3',
                                    width: 400,
                                }}
                            />
                           
                        </View>
                    <AndroidBackButton
                        onPress={() => this.onPressBack()}
                    />
                </Content>
            </Container>
        );
    }
}

export default SelectVendor;
